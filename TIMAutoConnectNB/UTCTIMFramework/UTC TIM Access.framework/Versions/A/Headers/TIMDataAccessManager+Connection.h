/*
 *
 File: TIMDataAccessManager+Connection.h
 Version: 1.0
 Description: TIMDataAccessManager+Connection is a part of TIM Access Framework.
                The header file exposes APIs are essential to establish a
                connection with TIM, if TIM is connected to iPad is discovered,
                handle connection/disconnection or if disconnection is initiated 
                from user side.
 
 -------------------------------------------------------------------------------
 
 THIS DOCUMENT IS THE PROPERTY OF UTC AEROSPACE SYSTEMS. YOU MAY NOT POSSESS,
 USE, COPY OR DISCLOSE THIS DOCUMENT OR ANY INFORMATION IN IT, FOR ANY PURPOSE,
 INCLUDING WITHOUT LIMITATION, TO DESIGN, MANUFACTURE OR REPAIR PARTS, OR OBTAIN
 ANY GOVERNMENT APPROVAL TO DO SO, WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS
 WRITTEN PERMISSION. NEITHER RECEIPT NOR POSSESSION OF THIS DOCUMENT ALONE,
 FROM ANY SOURCE, CONSTITUTES SUCH PERMISSION. POSSESSION, USE, COPYING OR
 DISCLOSURE BY ANYONE WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS WRITTEN PERMISSION
 IS NOT AUTHORIZED AND MAY RESULT IN CRIMINAL AND/OR CIVIL LIABILITY.
 
 -------------------------------------------------------------------------------
 
 Destination Control Marking:
 These commodities, technology or software are controlled from the United
 States in accordance with the Export Administration Regulations.
 Diversion contrary to U.S. law is prohibited.
 ECCN: 7E994
 
 -------------------------------------------------------------------------------
 *
 */

#ifndef UTC_TIM_Access_Library_TIMDataAccessManager_Connection_h
#define UTC_TIM_Access_Library_TIMDataAccessManager_Connection_h


@interface TIMDataAccessManager()
/*
 *  Starts accessory detection. Used for USB and Bluetooth TIM detection.
 */
-(void)startTIMDetection;

/*
 *  Reports back the list of detected TIMs / accessories. The
 *  'com.utc.utas.TIM.updatedConnectedTIMList' notification will be called back
 *  to indicate that this list has changed.
 */
@property(nonatomic, readonly) NSArray* accessoryList;

/*
 *  Allows a end user to poll if the client application has been connected to a
 *  TIM
 */
-(BOOL)isConnectedToAccessory;

/*
 *  Connects to an accessory selected from the accessoryList property.
 */
-(void)connectToAccessory:(EAAccessory*)connectedAccessory;

/*
 *  Used to start up accessory communication once an accessory has been
 *  connected. A client is responsible for calling this once they receive the
 *  connection successfull notification. This method initiates
 *  the TIM request and response cycle for protocol version negotiation until
 *  the ready signal arrives from the TIM. Once completes the kUTCTIMNotificationCanStartSendingRequests notification will received
 *  on to let a user know they can now use repository, print,
 *  aircraft data, or peer messaging function calls.
 */
-(void)startTIMDataCommunication;


/*
 * Performs disconnection from the connected TIM.
 */
-(void)disconnect;


@end

/******************TIM Connection Notifications********************/

/*
 *  Called whenever a TIM has been connected or disconnected to the USB/
 *  Bluetooth from the iOS device
 */
#define kUTCTIMNotificationUpdateConnectedTIMList @"com.utc.utas.TIM.updatedConnectedTIMList"

/*
 *  Called after sucessful connectToAccessory method call.
 */
#define kUTCTIMNotificationConnectionSuccessful @"com.utc.utas.TIM.connectionSuccessful"

/*
 *  Called after a call to startTIMDataCommunication message on
 *  TIMDataAccessManager has successfully completed.
 */
#define kUTCTIMNotificationCanStartSendingRequests @"com.utc.utas.TIM.canStartSendingRequests"

/*
 *  Called when TIM is disconnected from iPad.
 */
#define kUTCTIMDisconnected @"com.utas.TIM.disconnect"

#endif
