/*
 *
 File: TIMNotificationNames.h
 Version: 1.0
 Description: TIMNotificationNames is a part of TIM Access Framework. The header
              file exposes common notifications which come along with the TIM
              Access Framework irrespective of features purchased and opted by
              user.
 -------------------------------------------------------------------------------
 
 THIS DOCUMENT IS THE PROPERTY OF UTC AEROSPACE SYSTEMS. YOU MAY NOT POSSESS,
 USE, COPY OR DISCLOSE THIS DOCUMENT OR ANY INFORMATION IN IT, FOR ANY PURPOSE,
 INCLUDING WITHOUT LIMITATION, TO DESIGN, MANUFACTURE OR REPAIR PARTS, OR OBTAIN
 ANY GOVERNMENT APPROVAL TO DO SO, WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS
 WRITTEN PERMISSION. NEITHER RECEIPT NOR POSSESSION OF THIS DOCUMENT ALONE,
 FROM ANY SOURCE, CONSTITUTES SUCH PERMISSION. POSSESSION, USE, COPYING OR
 DISCLOSURE BY ANYONE WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS WRITTEN PERMISSION
 IS NOT AUTHORIZED AND MAY RESULT IN CRIMINAL AND/OR CIVIL LIABILITY.
 
 -------------------------------------------------------------------------------
 
 Destination Control Marking:
 These commodities, technology or software are controlled from the United
 States in accordance with the Export Administration Regulations.
 Diversion contrary to U.S. law is prohibited.
 ECCN: 7E994
 
 -------------------------------------------------------------------------------
 *
 */

#ifndef UTC_TIM_Access_Library_TIMNotificationNames_h
#define UTC_TIM_Access_Library_TIMNotificationNames_h


/********General Protocol Notifications********/

/*
 *   Notification name to allow an end user to see the raw commands being sent
 *   to the TIM for processing.
 */

#define kUTCTIMNotificationProtocolCommandRequestSent @"com.utc.utas.TIM.commandRequestSent"

/*
 *   Notification name to allow an end user to see the raw commands as they
 *   have been parsed on the TIM.
 */
#define kUTCTIMNotificationProtocolCommandResponseReceived @"com.utc.utas.TIM.commandResponseReceived"

/*
 *   Notification name for signaling that there is a change in request
 *   queue either due to addition of a new request or removal of an existing
 *   request on completion. Notification info contains list of requests 
 *   pending to be sent to TIM for processing. Each of these requests are 
 *   processed in serial order.
 */

#define kUTCTIMNotificationRequestQueueChange @"com.utc.utas.TIM.requestQueueChange"


/*
 *   Notification name for signaling bytes remaining to be transferred to the
 *   TIM.
 */
#define kUTCTIMNotificationRepositoryWriteBytesRemaining @"com.utc.utas.TIM.repositoryWriteBytesRemaining"



/*
 *   Notification received when some error occurrs (loss in communication
 *   with TIM, TIM rejected request, etc.)
 */
#define kUTCTIMError @"com.utas.TIM.error"

#endif
