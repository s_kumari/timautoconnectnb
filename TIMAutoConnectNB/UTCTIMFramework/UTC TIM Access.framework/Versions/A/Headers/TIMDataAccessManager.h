/*
 *
 File: TIMDataAccessManager.h
 Version: 1.0
 Description: TIMDataAccessManager is a part of TIM Access Framework. The
                header file exposes common APIs which come along with the TIM
                Access framework irrespective of features purchased and opted by
                user. This main file gives access to the singleton objects
                created by TIM Access Framework.
 
 -------------------------------------------------------------------------------
 
 THIS DOCUMENT IS THE PROPERTY OF UTC AEROSPACE SYSTEMS. YOU MAY NOT POSSESS,
 USE, COPY OR DISCLOSE THIS DOCUMENT OR ANY INFORMATION IN IT, FOR ANY PURPOSE,
 INCLUDING WITHOUT LIMITATION, TO DESIGN, MANUFACTURE OR REPAIR PARTS, OR OBTAIN
 ANY GOVERNMENT APPROVAL TO DO SO, WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS
 WRITTEN PERMISSION. NEITHER RECEIPT NOR POSSESSION OF THIS DOCUMENT ALONE,
 FROM ANY SOURCE, CONSTITUTES SUCH PERMISSION. POSSESSION, USE, COPYING OR
 DISCLOSURE BY ANYONE WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS WRITTEN PERMISSION
 IS NOT AUTHORIZED AND MAY RESULT IN CRIMINAL AND/OR CIVIL LIABILITY.
 
 -------------------------------------------------------------------------------
 
 Destination Control Marking:
 These commodities, technology or software are controlled from the United
 States in accordance with the Export Administration Regulations.
 Diversion contrary to U.S. law is prohibited.
 ECCN: 7E994
 
 -------------------------------------------------------------------------------
 *
 */

#import <ExternalAccessory/ExternalAccessory.h>

#import <Foundation/Foundation.h>
#import "TIMNotificationNames.h"



/*
 *  The TIMDataAccessManager is the root object that a client uses to
 *  access TIM.
 *  The methods in this class fire off standard notifications via
 *  NSNotificationCenter with [names and functionality]
 *  (TIMNotificationNames.h) as specified according to notification name.
 */
@interface TIMDataAccessManager : NSObject



/*
 *  Singleton accessor. Do not create the TIMDataAccessManager object to use this
 *  singleton accessor.
 *  This instance is responsible for doing different operations on TIM.
 */
+(TIMDataAccessManager*)sharedInstance;



/*
 *  Log File Access
 *  Provides a list of file paths to the underlying TIMDataAccessManager logs.
 *  These files are for now plain text cont and can be retrieved or rendered
 *  as needed
 */
-(NSArray*)getLogFileInfo;


@end


