//
//  AppDelegate.m
//  TIMAutoConnectNB
//
//  Created by Shilpa on 19/11/14.
//  Copyright (c) 2014 UTC Aerospace. All rights reserved.
//

#import "AppDelegate.h"
#import "AutoConnectDetailViewController.h"

@interface AppDelegate () <UISplitViewControllerDelegate>

@end

@implementation AppDelegate

@synthesize masterViewController;
@synthesize detailViewController;

void onUncaughtException(NSException* exception)
{
    NSLog(@"uncaught exception: %@", exception.description);
    
    for (int i=0; i < exception.callStackSymbols.count; i++)
    {
        NSLog(@"Stack Dump: %@", exception.callStackSymbols[i]);
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Display split view controller
    UISplitViewController *splitViewController = (UISplitViewController *)
    self.window.rootViewController;
    UINavigationController *navigationController =
    [splitViewController.viewControllers lastObject];
    splitViewController.delegate = (id)navigationController.topViewController;
    
    NSSetUncaughtExceptionHandler(&onUncaughtException);
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen]
                                                   bounds]];
    
    masterViewController = [[AutoConnectMasterViewController alloc]
                            initWithNibName:@"AutoConnectMasterViewController"
                            bundle:nil];
    UINavigationController *masterNavigationController =
    [[UINavigationController alloc]
     initWithRootViewController:masterViewController];
    
    detailViewController = [[AutoConnectDetailViewController alloc]
                            initWithNibName:@"AutoConnectDetailViewController"
                            bundle:nil];
    UINavigationController *detailNavigationController =
    [[UINavigationController alloc]
     initWithRootViewController:detailViewController];
    
    
    masterViewController.detailViewController = detailViewController;
    
    self.splitViewController = [[UISplitViewController alloc] init];
    self.splitViewController.delegate = detailViewController;
    self.splitViewController.viewControllers = @[masterNavigationController,
                                                 detailNavigationController];
    self.window.rootViewController = self.splitViewController;
    
    [self.window makeKeyAndVisible];
    
    return YES;

}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"applicationDidEnterBackground");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"applicationWillEnterForeground");

    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"applicationDidBecomeActive");

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"applicationWillTerminate");

    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    /*if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[AutoConnectDetailViewController class]] && ([(detailViewController *)[(UINavigationController *)secondaryViewController topViewController] detailItem] == nil)) {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    } else {
        return NO;
    }*/
    return YES;
}

@end
