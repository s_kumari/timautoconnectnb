/*
 *
 File: AutoConnectDetailViewController.m
 Version: 1.0
 Description: Displays the TIM connection status with the name of currently
              connected TIM (if connected).
 
 -------------------------------------------------------------------------------
 
 THIS DOCUMENT IS THE PROPERTY OF UTC AEROSPACE SYSTEMS. YOU MAY NOT POSSESS,
 USE, COPY OR DISCLOSE THIS DOCUMENT OR ANY INFORMATION IN IT, FOR ANY PURPOSE,
 INCLUDING WITHOUT LIMITATION, TO DESIGN, MANUFACTURE OR REPAIR PARTS, OR OBTAIN
 ANY GOVERNMENT APPROVAL TO DO SO, WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS
 WRITTEN PERMISSION. NEITHER RECEIPT NOR POSSESSION OF THIS DOCUMENT ALONE,
 FROM ANY SOURCE, CONSTITUTES SUCH PERMISSION. POSSESSION, USE, COPYING OR
 DISCLOSURE BY ANYONE WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS WRITTEN PERMISSION
 IS NOT AUTHORIZED AND MAY RESULT IN CRIMINAL AND/OR CIVIL LIABILITY.
 
 -------------------------------------------------------------------------------
 
 Destination Control Marking:
 These commodities, technology or software are controlled from the United
 States in accordance with the Export Administration Regulations.
 Diversion contrary to U.S. law is prohibited.
 ECCN: 7E994
 
 -------------------------------------------------------------------------------
 *
 */

#import "AutoConnectDetailViewController.h"

@interface AutoConnectDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation AutoConnectDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
        // Update the view.
        [self configureView];

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

#pragma mark -
#pragma mark View lifecycle

/****************************************************************************/
/*								View Lifecycle                              */
/****************************************************************************/

- (void)configureView
{
    
    self.appVersionLabel.textColor = [UIColor grayColor];
    self.appVersionLabel.text = [NSString stringWithFormat:@"Version: %@",
                                 [[[NSBundle mainBundle] infoDictionary]
                                  objectForKey:(NSString*)kCFBundleVersionKey]];
    
    //Since application is supporting background, it's safe to set TIM
    //connection status to disconnect everytime application is launched.
    [self UpdateUITIMStatusChange];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    self.title = @"TIM Auto Connect NB";
    
    //Registering for notifications recieved from
    //AutoConnectMasterViewController on connection, disconnection.
    [[NSNotificationCenter defaultCenter]
                    addObserver:self
                    selector:@selector(updateUIOnSuccessfullConnection)
                    name:@"UpdateUIOnConnectionSucessfull"
                    object:nil];
    
      [[NSNotificationCenter defaultCenter]
                            addObserver:self
                            selector:@selector(UpdateUITIMStatusChange)
                            name:@"UpdateUITIMStatusChange" object:nil];
    

}

#pragma mark -
#pragma mark Application Notifications & Methods

/****************************************************************************/
/*                       Application Notifications & Methods                */
/****************************************************************************/

-(void)updateUIOnSuccessfullConnection{
    
    self.connectionStatusLabel.text = [NSString stringWithFormat:
                                       @"Connection Status: Connected To '%@'",
                                       self.currentTIMConnectionName];
}

-(void)UpdateUITIMStatusChange{
    
    self.connectionStatusLabel.text = @"Connection Status: Disconnected";
    
}

#pragma mark - Split view Delegate

/****************************************************************************/
/*                      Split View Delegate Methods                         */
/****************************************************************************/

- (void)splitViewController:(UISplitViewController *)splitController
                    willHideViewController:(UIViewController *)viewController
                    withBarButtonItem:(UIBarButtonItem *)barButtonItem
                forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"TIM List", @"TIM List");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController
                    willShowViewController:(UIViewController *)viewController
                    invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view,
    //invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

@end
