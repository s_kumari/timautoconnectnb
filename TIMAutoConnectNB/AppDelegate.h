//
//  AppDelegate.h
//  TIMAutoConnectNB
//
//  Created by Shilpa on 19/11/14.
//  Copyright (c) 2014 UTC Aerospace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutoConnectMasterViewController.h"
#import "AutoConnectDetailViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UISplitViewController *splitViewController;

@property (strong, nonatomic) AutoConnectMasterViewController
*masterViewController;
@property (strong, nonatomic) AutoConnectDetailViewController
*detailViewController;


@end

