/*
 *
 File: AutoConnectMasterViewController.m
 Version: 1.0
 Description: Lists TIM discovered via USB, Bluetooth. Initiates connection
              with the first TIM in the list.
 
 -------------------------------------------------------------------------------
 
 THIS DOCUMENT IS THE PROPERTY OF UTC AEROSPACE SYSTEMS. YOU MAY NOT POSSESS,
 USE, COPY OR DISCLOSE THIS DOCUMENT OR ANY INFORMATION IN IT, FOR ANY PURPOSE,
 INCLUDING WITHOUT LIMITATION, TO DESIGN, MANUFACTURE OR REPAIR PARTS, OR OBTAIN
 ANY GOVERNMENT APPROVAL TO DO SO, WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS
 WRITTEN PERMISSION. NEITHER RECEIPT NOR POSSESSION OF THIS DOCUMENT ALONE,
 FROM ANY SOURCE, CONSTITUTES SUCH PERMISSION. POSSESSION, USE, COPYING OR
 DISCLOSURE BY ANYONE WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS WRITTEN PERMISSION
 IS NOT AUTHORIZED AND MAY RESULT IN CRIMINAL AND/OR CIVIL LIABILITY.
 
 -------------------------------------------------------------------------------
 
 Destination Control Marking:
 These commodities, technology or software are controlled from the United
 States in accordance with the Export Administration Regulations.
 Diversion contrary to U.S. law is prohibited.
 ECCN: 7E994
 
 -------------------------------------------------------------------------------
 *
 */

#import "AutoConnectMasterViewController.h"

#import "AutoConnectDetailViewController.h"

@interface AutoConnectMasterViewController () {
    BOOL _isTIMConnected;
}
@end

@implementation AutoConnectMasterViewController

#pragma mark -
#pragma mark View lifecycle

/****************************************************************************/
/*								View Lifecycle                              */
/****************************************************************************/

- (void)awakeFromNib
{
    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    self.detailViewController = (AutoConnectDetailViewController *)
    [[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.title = @"TIM list";
    
    
    //Set TIM connected status to 'NO'.
    _isTIMConnected = NO;
    
    //This notification is handled to make UI changes when application enters
    //foreground.
    [[NSNotificationCenter defaultCenter] addObserver: self selector:
     @selector(handleEnteringInactiveState:)
                                                 name: @"UIApplicationWillResignActiveNotification" object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:
     @selector(handleEnteringActiveState:)
                                                 name: @"UIApplicationDidBecomeActiveNotification" object: nil];
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework on issuing method "startTIMDetection". This
    //notificaiton is also received whenever TIM list gets updated
    /*[[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateConnectedTIMList:)
     name:kUTCTIMNotificationUpdateConnectedTIMList
     object:nil];
    
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework on issuing method "connectToAccessory".
    [[NSNotificationCenter defaultCenter] addObserver:self selector:
     @selector(connectionSuccessful:)
                                                 name:kUTCTIMNotificationConnectionSuccessful object:nil];
    
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework issuing method "startTIMDataCommunication".
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(canStartSendingRequests:)
     name:kUTCTIMNotificationCanStartSendingRequests
     object:nil];
    
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework on disconnection from iPad. The disconnection
    //can be because of external interuption( bluetooth signal lost, etc) or
    //user initiated (by "disconnect" method call).
    [[NSNotificationCenter defaultCenter] addObserver:self selector:
     @selector(TIMDisconnected:)
                                                 name:kUTCTIMDisconnected object:nil];
    
    [[TIMDataAccessManager sharedInstance] startTIMDetection];*/

    //Start the TIM detection by calling "startTIMDetection" API method of
    //UTC TIM Access Framework. This method is responsible to detect all the
    //connected TIM('s) to iPad and populates the "accessoryList" array.
    //[[TIMDataAccessManager sharedInstance] startTIMDetection];
}


#pragma mark -
#pragma mark Application notification

/****************************************************************************/
/*                            Application notification                      */
/****************************************************************************/

-(void) handleEnteringInactiveState:(NSNotification*)notification{

    if (_isTIMConnected) {
        [[TIMDataAccessManager sharedInstance] disconnect];
        
    }
    NSLog(@"Entering handleEnteringInactiveState");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUTCTIMDisconnected object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUTCTIMNotificationCanStartSendingRequests object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUTCTIMNotificationConnectionSuccessful object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUTCTIMNotificationUpdateConnectedTIMList object:nil];

}

-(void) handleEnteringActiveState:(NSNotification*)notification{
    
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework on issuing method "startTIMDetection". This
    //notificaiton is also received whenever TIM list gets updated
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateConnectedTIMList:)
     name:kUTCTIMNotificationUpdateConnectedTIMList
     object:nil];
    
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework on issuing method "connectToAccessory".
    [[NSNotificationCenter defaultCenter] addObserver:self selector:
     @selector(connectionSuccessful:)
                                                 name:kUTCTIMNotificationConnectionSuccessful object:nil];
    
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework issuing method "startTIMDataCommunication".
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(canStartSendingRequests:)
     name:kUTCTIMNotificationCanStartSendingRequests
     object:nil];
    
    
    //Register current View Controller with the notification recieved from
    //UTC TIM Access Framework on disconnection from iPad. The disconnection
    //can be because of external interuption( bluetooth signal lost, etc) or
    //user initiated (by "disconnect" method call).
    [[NSNotificationCenter defaultCenter] addObserver:self selector:
     @selector(TIMDisconnected:)
                                                 name:kUTCTIMDisconnected object:nil];
    
    [[TIMDataAccessManager sharedInstance] startTIMDetection];

    NSLog(@"Entering handleEnteringActiveState");
    
}

#pragma mark -
#pragma mark UTC TIM Access notifications

/****************************************************************************/
/*                      UTC TIM Access Notification                         */
/****************************************************************************/

/*
 * This notification keeps triggering whenever TIM list
 * ([[TIMDataAccessManager sharedInstance].accessoryList)changes due to
 * connection/disconnection of iOS Device with TIM accessory.
 *
 * notification: This argument contains no data.
 *
 * The TIM connection status is checked and if not connected then based on the
 * "accessoryList" attempt to connect to first TIM in the list is made in this
 * method.
 *
 */
-(void)updateConnectedTIMList:(NSNotification*)notification {
    
    
    //Logging total no of TIM('s) discovered.
    NSLog(@"updateConnectedTIMList called, total accessory count= %lu",
          (unsigned long)
          [TIMDataAccessManager sharedInstance].accessoryList.count);
    
    //If not already connected, initiate connection with TIM.
    if (!_isTIMConnected) {
        //If there are TIM('s) discovered.
        if ([TIMDataAccessManager sharedInstance].accessoryList.count > 0) {
                // Connecting to the first TIM in list.
                [[TIMDataAccessManager sharedInstance] connectToAccessory:
                 [[TIMDataAccessManager sharedInstance].accessoryList
                                                            objectAtIndex:0] ];
            
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"UpdateUITIMStatusChange" object:nil
                 userInfo:[NSDictionary dictionaryWithObject:@"Connecting..."
                                                      forKey:@"status"]];
            
                self.detailViewController.currentTIMConnectionName =
                        [[[TIMDataAccessManager sharedInstance].accessoryList
                                                        objectAtIndex:0] name];
        }
    }
    
    //Reload the data for the tablview used to display the current
    //discovered TIM(s) to ipad.
    [self.tableView reloadData];
    
}

/*
 * This notification is received when device is connected successfully to TIM.
 * connectToAccessory: method call triggers this notification
 *
 * notification: This argument contains no data
 */
-(void)connectionSuccessful:(NSNotification*)notification {
    
    NSLog(@"connectionSuccessful");

    //Now that we have our request / response view controller revved up, and a
    //connected TIM, start up the TIM Data Communication cycles.
    [[TIMDataAccessManager sharedInstance] startTIMDataCommunication];
}

/*
 * This notification is received when device is ready to communicate with the
 * connected TIM.
 *
 * notification: This argument contains no data.
 *
 */
-(void)canStartSendingRequests:(NSNotification*)notification {
    
    //Update TIM connection status.
    _isTIMConnected = YES;
    NSLog(@"Connected");
    //Updating AutoConnectDetailViewController UI to display TIM connection
    //status.
    [[NSNotificationCenter defaultCenter] postNotificationName:
                @"UpdateUIOnConnectionSucessfull" object:nil userInfo:nil];
    
}

/*
 * This notifies about TIM disonnection from the device.
 *
 * notification: This argument has userInfo NSDictionary with "DisconnectIssue",
 * "SpecificError": This key contains usefull information.
 * "DisconnectIssue":Provides the reason for disconnect notification.
 * "SpecificError": Provides the information about error that caused disconnect.
 */
-(void)TIMDisconnected:(NSNotification*)notification {
    
    //Update TIM connection status.
    _isTIMConnected = NO;
    NSLog(@"Disconnected");

    //Updating AutoConnectDetailViewController UI to display TIM connection
    //status.
    [[NSNotificationCenter defaultCenter] postNotificationName:
                                        @"UpdateUITIMStatusChange" object:nil
                    userInfo:[NSDictionary dictionaryWithObject:@"Disconnected"
                                                         forKey:@"status"]];
}


#pragma mark -
#pragma mark TableView Delegates/DataSources

/****************************************************************************/
/*					TableView Delegates/DataSources							*/
/****************************************************************************/


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/*
 * Gets the count of no of TIM accessories discovered.
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [TIMDataAccessManager sharedInstance].accessoryList.count;
}

/*
 * Set the height of table row.
 */
- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

/*
 * Gets the discovered TIM accessories information and display in table.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:
                UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    id accessoryForCell = [[TIMDataAccessManager sharedInstance].accessoryList
                           objectAtIndex:indexPath.row];
    
    NSMutableString* protocolStrings = [NSMutableString stringWithCapacity:200];
    for (NSString* protocolString in [accessoryForCell protocolStrings])
    {
        [protocolStrings appendFormat:@"%@  ", protocolString];
    }
    
    cell.textLabel.text = [accessoryForCell name];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    cell.detailTextLabel.numberOfLines = 6;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Manufacturer:%@\nModel Number:%@\nSerial Number:%@\nFirmware Rev:%@\nHardware Revision:%@\n%@",
                                 [accessoryForCell manufacturer],
                                 [accessoryForCell modelNumber],
                                 [accessoryForCell serialNumber],
                                 [accessoryForCell firmwareRevision],
                                 [accessoryForCell hardwareRevision],
                                 protocolStrings];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TIM Auto Connect NB"
                                                    message:@"This is sample app to demo auto connect to first available TIM in the list.\n Connection to TIM by user selection is not supported."
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
