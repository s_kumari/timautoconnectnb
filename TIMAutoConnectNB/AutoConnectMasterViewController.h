/*
 *
 File: AutoConnectMasterViewController.h
 Version: 1.0
 Description: Header file.
 
 -------------------------------------------------------------------------------
 
 THIS DOCUMENT IS THE PROPERTY OF UTC AEROSPACE SYSTEMS. YOU MAY NOT POSSESS,
 USE, COPY OR DISCLOSE THIS DOCUMENT OR ANY INFORMATION IN IT, FOR ANY PURPOSE,
 INCLUDING WITHOUT LIMITATION, TO DESIGN, MANUFACTURE OR REPAIR PARTS, OR OBTAIN
 ANY GOVERNMENT APPROVAL TO DO SO, WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS
 WRITTEN PERMISSION. NEITHER RECEIPT NOR POSSESSION OF THIS DOCUMENT ALONE,
 FROM ANY SOURCE, CONSTITUTES SUCH PERMISSION. POSSESSION, USE, COPYING OR
 DISCLOSURE BY ANYONE WITHOUT UTC AEROSPACE SYSTEMS'S EXPRESS WRITTEN PERMISSION
 IS NOT AUTHORIZED AND MAY RESULT IN CRIMINAL AND/OR CIVIL LIABILITY.
 
 -------------------------------------------------------------------------------
 
 Destination Control Marking:
 These commodities, technology or software are controlled from the United
 States in accordance with the Export Administration Regulations.
 Diversion contrary to U.S. law is prohibited.
 ECCN: 7E994
 
 -------------------------------------------------------------------------------
 *
 */


#import <UIKit/UIKit.h>
#import <UTC TIM Access/TIMDataAccessManager.h>
#import <UTC TIM Access/TIMDataAccessManager+Connection.h>

@class AutoConnectDetailViewController;

@interface AutoConnectMasterViewController : UITableViewController

@property (strong, nonatomic) AutoConnectDetailViewController
                                                        *detailViewController;

@end
